vim-autocomplpop
================

## About

`vim-autocomplpop` is a fork of [AutoComplPop plugin by Takeshi NISHIDA](https://www.vim.org/scripts/script.php?script_id=1879) tweaked to some personal preferences. All the credits naturally go to the actual author. Please, install the original version.

## Notes and changes

  - No auto-selecting the first (or the last) candidate;
  - Tags can be completed, too, if added to the `g:acp_completeOption` as described in `:help 'complete'`, like this:

        let g:acp_completeOption = '.,]'


